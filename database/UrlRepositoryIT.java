package org.steinko.shortcode;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Iterator;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;


@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)

public class UrlRepositoryIT  {
	
		
	@Autowired
	private UrlRepository repository;
	
	
	@Test 
	public void shouldNotBeNull () {
		assertNotNull(repository);
	}
	
	
	@Test
	public void shouldSave() {
		
		repository.save(new Url("Stein"));
		assertNotNull(repository.findAll());
	}
	
	
	@Test
	public void shouldDelete() {
		
		repository.save(new Url("Stein"));
		Iterable<Url> urls = repository.findAll();
		Iterator<Url> iterator = urls.iterator();
		Url url = iterator.next();
		repository.delete(url);
		assertEquals(repository.count(), 0L);
	}

}
