package org.steinko.shortcode;

import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.JpaRepository;

@Repository
interface UrlRepository extends  JpaRepository<Url, Long> {}