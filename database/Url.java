package org.steinko.shortcode;

import javax.persistence.Entity;  
import javax.persistence.Id; 

@Entity 
public class Url {
	
	@Id
	String shortCode;
	
	protected Url(){};
	
	public Url (String shortCode)  { 
		 this.shortCode = shortCode;
				 }
	
	public String getShortCode() {
		return shortCode;
	}
	
}