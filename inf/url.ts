mport * as aws from "@pulumi/aws";

export const maxId = new aws.dynamodb.Table("url", {
    attributes: [
                {
                    name: "shortcode",
                     type: "S",
                 }
     ],
    hashKey: "shortcode",
    name: "url"
    })