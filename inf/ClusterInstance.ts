import * as aws from "@pulumi/aws";
import {cluster} from './Cluster'


export const clusterInstance = new aws.rds.ClusterInstance("exampleClusterInstance", {
    clusterIdentifier: cluster.id,
    instanceClass: "db.serverless",
 });