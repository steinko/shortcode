import * as pulumi from "@pulumi/pulumi"
import * as aws from "@pulumi/aws";

export const cluster = new aws.rds.Cluster("aurora-postgresql", {
    databaseName: "MyDatabase",
    engine: "aurora-postgresql",
    masterPassword: pulumi.secret("RoxyMusic11!!"),
    masterUsername: "steinko",
    engineMode:"serverless",
    enableHttpEndpoint:true,
    enabledCloudwatchLogsExports:[ "postgresql"],
    clusterIdentifier: "cluster"
});