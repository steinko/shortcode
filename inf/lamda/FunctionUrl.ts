import * as aws from "@pulumi/aws"
import {lamdaFunction} from "./LamdaFunction"

export const functionUrl = new aws.lambda.FunctionUrl("function-url", {
    functionName: lamdaFunction.name,
    authorizationType: "NONE",
});