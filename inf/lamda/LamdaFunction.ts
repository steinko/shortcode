import * as aws from "@pulumi/aws"
import * as pulumi from "@pulumi/pulumi";
import  {executionRole} from "./ExecutionRole"

const handler= "org.steinko.shortcode.LambdaHandler::handleRequest"


export const lamdaFunction = new aws.lambda.Function("lamda-function", {
    code: new pulumi.asset.FileArchive("../app/build/libs/shortcode-0.0.1-all.jar"),   
    runtime: aws.lambda.Runtime.Java11,
    handler: handler,
    role: executionRole.arn,
    name: "uppercase",
    memorySize: 3000,
    timeout: 900,
    tracingConfig:{mode:"Active"},
    architectures:["arm64"],
    layers: [ "arn:aws:lambda:eu-north-1:901920570463:layer:aws-otel-java-agent-arm64-ver-1-24-0:1",
              "arn:aws:lambda:eu-north-1:464622532012:layer:Datadog-Extension-ARM:43"], 
    environment: {
        variables: {
			AWS_LAMBDA_EXEC_WRAPPER: "/opt/otel-handler",
			DD_LOGS_INJECTION:"true",
			DD_SITE: "datadoghq.eu",
			DD_ENV: "dev",
			DD_API_KEY: "9fa2ebdddb1632f6d0d5a5df856ec554"
		}
			
        }

},
   )
