import * as aws from "@pulumi/aws";

export const maxId = new aws.dynamodb.Table("maxId", {
    attributes: [{
                   name: "id",
                   type: "N",
                 }, 
                {
                    name: "shortcode",
                     type: "N",
                 }
     ],
    hashKey: "id",
    name: "maxId"
    })