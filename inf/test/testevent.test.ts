
import { client, v2 } from "@datadog/datadog-api-client";

describe("datadog testevent", () => {

  xit("should exist a test event",async () => {
	  
	  const configurationOpts = {
                                   authMethods: {
                                                  apiKeyAuth: "0efca8ceab2f524bbc5c9ed66e5319b1",
                                                  appKeyAuth: "2dc3b3a920c6ad6a33abada60df18f39ed140eb6"
                                                },
                                   debug: true
                                
         
            };

         const configuration = client.createConfiguration(configurationOpts);
         client.setServerVariables(configuration, { site: "datadoghq.eu"});
         const apiInstance = new v2.CIVisibilityTestsApi(configuration);
         const data:v2.CIAppTestEventsResponse  =  await apiInstance.searchCIAppTestEvents()
         expect(data).toBe("")
  
    })
  })