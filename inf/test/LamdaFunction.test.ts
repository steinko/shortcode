import * as pulumi from "@pulumi/pulumi";

pulumi.runtime.setMocks({
    newResource: function(args: pulumi.runtime.MockResourceArgs): {id: string, state: any, } {
        return {
            id: args.inputs.name + "_id",
            state: args.inputs,
        };
    },
    call: function(args: pulumi.runtime.MockCallArgs) {
        return args.inputs;
    },
},
  "project",
  "stack",
  false, // Sets the flag `dryRun`, which indicates if pulumi is running in preview mode.
);



describe("Lamda Function", ()=> {
    let infra: typeof import("../lamda/LamdaFunction");

    beforeAll(async ()=> {    
        infra = await import("../lamda/LamdaFunction");
    })

    describe("lamada function", () => {
        it("should have a name", ()=> {
          pulumi.all([infra.lamdaFunction.name]).apply(([name]) => {
                        expect(name).toBe("uppercase")
                     })
       })
       
    })
})
