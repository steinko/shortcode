DD_SITE=datadoghq.eu DD_TRACER_VERSION=1.40.0  DD_LOGS_INJECTION=true DD_SERVERLESS_APPSEC_ENABLED=true  ./gradlew cleanTest test --rerun-tasks \
-Dorg.gradle.jvmargs=-javaagent:$HOME/.datadog/dd-java-agent-1.14.0.jar=\
dd.civisibility.enabled=true,\
dd.civisibility.agentless.enabled=true,\
dd.env=$DD_ENV,\
dd.service=uppercase

