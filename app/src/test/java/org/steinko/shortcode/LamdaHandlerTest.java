package org.steinko.shortcode;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class LamdaHandlerTest {
	
	@Test
	void shouldDeliverUppercase()
	{   

		LambdaHandler lambdaHandler = new LambdaHandler();
		String respons = lambdaHandler.handleRequest("Hello World");
		assertEquals(respons, "HELLO WORLD");
	}	
}