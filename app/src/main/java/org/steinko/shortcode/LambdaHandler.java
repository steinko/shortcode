package org.steinko.shortcode;



import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class LambdaHandler  {
	
	    private static final Logger logger = LogManager.getLogger(LambdaHandler.class);
	    
	    
		public String handleRequest(String request)
		{  
			// System.out: One log statement but with a line break (AWS Lambda writes two events to CloudWatch).
	        System.out.println("log data from stdout \n this is continuation of system.out");

	       // System.err: One log statement but with a line break (AWS Lambda writes two events to CloudWatch).
	        System.err.println("log data from stderr. \n this is a continuation of system.err");

	        // Use log4j to log the same thing as above and AWS Lambda will log only one event in CloudWatch.
	        logger.debug("log data from log4j debug \n this is continuation of log4j debug");

	        logger.error("log data from log4j err. \n this is a continuation of log4j.err");

			logger.info("Serving lambda request.");
			String respons = request.toUpperCase();	
			return respons;
			
		}

}